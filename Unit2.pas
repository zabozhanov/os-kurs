unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  Point = class
  public
    x: Real;
    y: Real;
    constructor Create(x: Real; y: Real);
    procedure normalize;
    function length(): real;
    procedure mul(mul:Real);
    function distance(p:Point):real;
  end;
type
  Task = class(TPanel)
  public
    text: TLabel;
    location: Point;
    constructor Create(parent: TWinControl; number: Integer);
    procedure updatePosition;
  end;

implementation

constructor Point.Create(x: real; y: real);
begin
  self.x := x;
  self.y := y;
end;

function Point.length(): real;
var
  d: real;
begin
  d := self.x * self.x + self.y * self.y;
  if (d > 0) then
    Result := Sqrt(d)
  else
    Result := -1;
end;

procedure Point.normalize;
var
  length: real;
begin
  length := self.length;
  if (length > 0) then
  begin
    self.x := self.x / length;
    self.y := self.y / length;
  end;
end;

procedure Point.mul(mul:real);
begin
  self.x := self.x * mul;
  self.y := self.y * mul;
end;

function Point.distance(p:Point):real;
begin
  Result := Point.Create(p.x - self.x, p.y - self.y).length;
end;

constructor Task.Create(parent: TWinControl; number: Integer);
begin
  inherited Create(parent);
  self.DoubleBuffered := True;
  self.Width := 190;
  self.Height := 65;
  self.location := Point.Create(216, 384);
  self.BevelOuter := bvNone;
  self.Color := clRed;
  self.Parent := parent;
  self.text := TLabel.Create(Self);
  self.text.Caption := 'Process #' + IntToStr(number);
  self.text.Parent := self;
  self.text.width := self.Width;
  self.text.Height := self.Height;
  self.text.Alignment := taCenter;
  Self.updatePosition;
end;

procedure Task.updatePosition;
begin
  self.Left := Round(self.location.x);
  self.Top := Round(self.location.y);
end;

end.

