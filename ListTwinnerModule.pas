unit ListTwinnerModule;

interface

uses Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Unit3, Unit2;

type
  IListTwinnerListener = interface
    procedure listTwinnerEnded;
  end;
  ListTwinner = class(TComponent, ITwinnerListener)
  public
    constructor CreateWith(objects: TList; listener: IListTwinnerListener);
    procedure twinnerEnded(sender: Twinner);
    procedure start;
  private
    listener: IListTwinnerListener;
    objects: TList;
    endedTwinnersCount: Integer;
  end;

implementation

constructor ListTwinner.CreateWith(objects: TList; listener: IListTwinnerListener);
begin
  self.listener := listener;
  self.objects := objects;
end;

procedure ListTwinner.start;
var
  i: Integer;
  tw: Twinner;
  moveto: point;
  curTask: Task;
begin
  endedTwinnersCount := 0;
  if (self.objects.Count = 0) then self.listener.listTwinnerEnded;
  for i := 0 to self.objects.Count - 1 do
  begin
    curTask := self.objects[i];
    moveto := Point.Create(curTask.location.x, curTask.location.y);
    moveto.y := moveto.y + curTask.Height + 5;
    tw := Twinner.create(curTask, moveto, 1, self);
    tw.start;
  end;
end;

procedure ListTwinner.twinnerEnded(sender: Twinner);
begin
  Inc(endedTwinnersCount);
  if (endedTwinnersCount =  self.objects.Count) then begin
    self.listener.listTwinnerEnded;
  end;
end;

end.

