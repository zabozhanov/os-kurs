unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Unit2, unit3, ListTwinnerModule;

type
  TForm1 = class(TForm, ITwinnerListener, IListTwinnerListener)
    pnl2: TPanel;
    btn1: TButton;
    scrlbx1: TScrollBox;
    procedure FormCreate(Sender: TObject);
    procedure btn1Click(Sender: TObject);
  private
  public
    procedure twinnerEnded(sender: Twinner);
    procedure listTwinnerEnded;
  end;

var
  Form1: TForm1;
  tasks: TList;
  curTask: Task;
  curTwinner: Twinner;
  listT:ListTwinner;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  tasks := TList.Create;
  listT := ListTwinner.CreateWith(tasks, self);
  self.DoubleBuffered := true;
end;

procedure TForm1.btn1Click(Sender: TObject);
begin      
  curTask := Task.Create(self, tasks.Count);
  listT.start;
end;

procedure TForm1.twinnerEnded(sender: Twinner);
begin 
  if (sender = curTwinner) then begin
    sender.obj.Parent := scrlbx1;
    sender.obj.location := Point.Create(5, sender.obj.Top - scrlbx1.Top);
    sender.obj.updatePosition;
    tasks.Add(curTask);
  end;
end;

procedure TForm1.listTwinnerEnded;
begin
  curTwinner := Twinner.Create(curTask, Point.Create(self.scrlbx1.Left + 5,
    self.scrlbx1.Top + 5), 2, Self);
  curTwinner.start;
end;

end.

