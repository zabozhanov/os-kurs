unit Unit3;

interface

uses Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Unit2;
type
  Twinner = class;
  ITwinnerListener = interface
    procedure twinnerEnded(sender: Twinner);
  end;

  Twinner = class
  public          
    obj: Task;
    constructor Create(obj: Task; moveTo: Point; time: real; listener:
      ITwinnerListener);
    procedure start;
    procedure stop;
  private
    listener: ITwinnerListener;
    moveTo: Point;
    time: real;
    speed: Point;                    
    timer: TTimer;
    procedure OnMyTimer(Sender: TObject);
  end;


implementation

constructor Twinner.Create(obj: Task; moveTo: Point; time: real; listener:
  ITwinnerListener);
var sp:real;
begin
  self.listener := listener;
  self.moveTo := moveTo;
  self.obj := obj;
  self.time := time;
  if (timer = nil) then begin
  timer := TTimer.Create(nil);
  timer.OnTimer := self.OnMyTimer;
  timer.Interval := 50;
  end;

  self.speed := Point.Create(moveTo.x - obj.location.x, moveTo.y - obj.location.y);
  sp := self.speed.length / time;
  self.speed.normalize;
  self.speed.mul(sp/1000 * timer.Interval);
end;

procedure Twinner.start;
begin
  timer.Enabled := True;
end;

procedure Twinner.stop;
begin
  timer.Enabled := false;
  timer.Free;
  timer := nil;
  self.obj.location.x := self.moveTo.x;
  self.obj.location.y := self.moveTo.y;
  self.obj.updatePosition;
  self.listener.twinnerEnded(Self);
end;

procedure Twinner.OnMyTimer(Sender: TObject);
begin
  self.obj.location.x := Self.obj.location.x + Self.speed.x;
  Self.obj.location.y := Self.obj.location.y + Self.speed.y;
  Self.obj.updatePosition;
  if (Self.obj.location.distance(Self.moveTo) < 20) then begin
   Self.stop;
  end;
end;
end.

