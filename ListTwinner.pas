unit ListTwinnerModule;

interface

uses Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Unit3;

type
  IListTwinnerListener = interface
    procedure listTwinnerEnded;
    end;
  ListTwinner = class (ITwinnerListener)
  constructor Create(objects:TList);
  end;

implementation

end.
